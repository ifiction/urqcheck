#!/usr/bin/ruby
require 'ffi/hunspell'

if not ARGV.first then
  puts "Нужен аргумент"
  exit
end

folder = ARGV[0].chomp
if (folder[0] != '/') then
  folder_path = File.join(Dir.pwd, folder)
else
  folder_path = folder.chomp('/')
end
unless File.directory?(folder_path)
  puts "Ошибка: #{folder_path} не является директорией."
  exit
end

def spell(words)
  FFI::Hunspell.dict('ru_RU') do |dict|
    words.each do |word|
      if word.match(/^[^А-Яа-я]+$/) then # в строке нет русских символов
        next
      end
      if word.chr.upcase.match(/[А-Я]/) then # не проверять слова с заглавных
        next
      end
      word = word.downcase.tr('"\'«»[]()?!%.:…,\#\$\/0123456789', "")
      if not dict.check?(word) then
        puts "Ошибка в слове #{word} в строке #{out}"
      end
    end
  end
end

def check_spelling(file)
  cp1251content = File.read(file)
  content = cp1251content.encode("UTF-8", "CP1251")

  content.each_line do |line|
    if line.match("TEXT") then
      output = line.scan(/"([^"]*)"/)
      output.each do |out|
        words = out.first.split(/(\s|:|\#\/\$)/)
        spell(words)
      end
    end
    if line.match(/^\s*(pn|pln)\s+/) then
      output = line.scan(/^\s*(p|pln|btn)(.*)/)
      output.each do |out|
        words = out.first.split(/(\s|:|\#\/\$)/)
        spell(words)
      end
    end
  end
end

Dir.glob([
  folder_path + '/**/*.qst', 
  folder_path + '/**/*.urq', 
]).each do |file|
  puts " --- Проверка #{file} ---"
  check_spelling(file)
end
